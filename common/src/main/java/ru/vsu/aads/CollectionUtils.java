package ru.vsu.aads;

import java.util.Collection;
import java.util.stream.Collectors;

public class CollectionUtils {
    private CollectionUtils() {

    }

    public static String toString(Collection<?> collection, String separator) {
        return collection.stream().map(Object::toString).collect(Collectors.joining(separator));
    }

    public static void print(Collection<?> collection, String separator) {
        System.out.println(toString(collection, separator));
    }
}
