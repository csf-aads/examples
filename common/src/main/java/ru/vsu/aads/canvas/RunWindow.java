package ru.vsu.aads.canvas;

public class RunWindow {
    public static void main(String[] args) {
        PaintForm paintForm = new PaintForm();
        paintForm.setSize(1500, 1500);
        paintForm.setVisible(true);
    }
}
