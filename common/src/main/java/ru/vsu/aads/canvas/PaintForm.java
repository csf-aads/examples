package ru.vsu.aads.canvas;

import javax.swing.*;
import java.awt.*;

public class PaintForm extends JFrame {
    private JPanel panel1;

    public PaintForm() throws HeadlessException {
        super();
        panel1.setSize(1500, 1500);
        setContentPane(panel1);
        panel1.add(new Canvas());
    }
}
