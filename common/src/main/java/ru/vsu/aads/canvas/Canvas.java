package ru.vsu.aads.canvas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

public class Canvas extends JPanel implements MouseListener, MouseMotionListener {

    public static final int CELL_SIZE = 50;
    Image img = new BufferedImage(1500, 1500, BufferedImage.TYPE_INT_RGB);      // Contains the image to draw on MyCanvas


    int[][] maze = new int[30][30];

    public Canvas()
    {
        this.setSize(1500, 1500);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    public void paintComponent(Graphics g)
    {
        // Draws the image to the canvas
        g.drawImage(img, 0, 0, null);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
//        int x = e.getX();
//        int y = e.getY();
//
//        Graphics g = img.getGraphics();
//        Color color = g.getColor();
//        g.setColor(Color.WHITE);
//        g.fillOval(x, y, 50, 50);
//        g.dispose();
//        g.setColor(color);
//        this.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        int col = e.getX() / CELL_SIZE;
        int row = e.getY() / CELL_SIZE;
        int x = col * CELL_SIZE;
        int y = row * CELL_SIZE;

        maze[row][col] = SwingUtilities.isLeftMouseButton(e) ? 1 : 0;

        Graphics g = img.getGraphics();
        Color color = g.getColor();
        g.setColor(SwingUtilities.isLeftMouseButton(e) ? Color.WHITE : Color.BLACK);
        g.fillRect(x, y, CELL_SIZE, CELL_SIZE);
        g.dispose();
        g.setColor(color);
        this.repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
