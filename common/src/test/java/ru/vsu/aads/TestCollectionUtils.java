package ru.vsu.aads;


import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestCollectionUtils {

    @Test
    void testListPrint() {
        List<Integer> integers = List.of(1, 2, 3);
        String expected = "1, 2, 3";
        String string = CollectionUtils.toString(integers, ", ");
        assertEquals(expected, string);
    }
}
